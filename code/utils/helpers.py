import numpy as np
import matplotlib.pyplot as plt
import cv2


# =====================================================================
# =====================================================================



def see(im):
    try:
        cv2.namedWindow('image',cv2.WINDOW_NORMAL)
        cv2.resizeWindow('image', 900,650)
        cv2.imshow('image',im)
    except:
        print("INPUT ERROR: not an image or not in correct format for opencv") 
    cv2.waitKey(0)
    cv2.destroyAllWindows()


# =====================================================================
# =====================================================================


def look(im):
    plt.gray()
    fig = plt.figure(figsize = (10,10))
    if len(im.shape) > 2:
        plt.imshow(im[:,:,::-1])
    else:
        plt.imshow(im)
    plt.show()


# =====================================================================
# =====================================================================



def im2cv(im):
    im = 255*im
    im2 = im.astype(np.uint8)
    return im2


# =====================================================================
# =====================================================================
