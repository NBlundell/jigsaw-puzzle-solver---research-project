import numpy as np
import cv2
import matplotlib.pyplot as plt
from skimage import feature
from skimage.measure import label
from skimage.color import label2rgb
import math
import imutils
from utils.helpers import *
from puzzles import puzzle_functions as pf


class Piece():
    
    """
    Class to store a single puzzle piece.


    class attributes:
        sides array: array of contours for each side - order is up,left,down,right
        types array: array of ints - straight=0, indent=-1, outdent=1
        number: label number
        cx, cy: centre coords
        piece_type: corner, edge, inner
        thresh: thresholded piece image
        N,M: dimensions of input image
        angle: angle of rotation of piece
    """
    

    def __init__(self, img, label_img, label_num, show_imgs=False):

        """
        Separate piece into 4 sides and classify them as straight, indent or outdent.
        Use the side types to classify piece as edge, corner or interior.

        """

        self.number = label_num
        
        self.N = img.shape[0]
        self.M = img.shape[1]
        
# use segmented image to get shape of piece
        disp = np.zeros(label_img.shape[:2]).astype(np.uint8)
        disp[label_img[:,:]==label_num] = 255
        if show_imgs:
            see(disp)
# get contours
        contours, _ = cv2.findContours(disp, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        if len(contours) > 1:
            for i in range(len(contours)):
                if len(contours[i]) > 0.15*min(self.N, self.M):
                    contour = contours[i]
        else:
            contour = contours[0]

# get centre of contour
        M = cv2.moments(contour)
        self.cx = int(M['m10']/M['m00'])
        self.cy = int(M['m01']/M['m00'])

            
# translate and rotate shape
        temp = pf.translate_to_centre(disp, self.cx, self.cy)
        self.angle = pf.get_angle(contour)
        self.thresh= pf.rotate_img(temp, self.angle)
        if show_imgs:
            see(self.thresh)
            
            
# get 4 sides as contours, order them, and classify them
        sides_unordered = pf.get_sides(self.thresh, printing=show_imgs)
        self.sides = pf.order_sides(sides_unordered)
        if show_imgs:
            shown = np.zeros(img.shape)
            cv2.drawContours( shown, self.sides[0], -1, (255,0,255), 2); # magenta, top
            cv2.drawContours( shown, self.sides[1], -1, (0,0,255), 2); # red, left
            cv2.drawContours( shown, self.sides[2], -1, (255,0,0), 2); # blue, down
            cv2.drawContours( shown, self.sides[3], -1, (0,255,0), 2); # green, right

            see(shown)
    
        self.types = pf.side_types(self.sides, min(self.N, self.M))
        if show_imgs:
            print(self.types)
        
# classify piece
        straights = self.types.count(0)
        if straights == 2:
            self.piece_type = "corner"
        elif straights == 1:
            self.piece_type = "edge"
        else:
            self.piece_type = "inner"
            
        if show_imgs:
            print(self.piece_type)
        

# =========================== METHODS =======================================
# ===========================================================================


    def get_picture(self, jigsaw):

        """
        Returns the centred and rotated image of the piece on its own.

        """

        temp2 = np.zeros(jigsaw.image.shape)
        temp2[jigsaw.piece_image==self.number] = 1.0
        temp2 = temp2*(jigsaw.image/255.0)
        tr = pf.translate_to_centre(temp2, self.cx, self.cy)
        picture = pf.rotate_img(tr, self.angle)
        # see(picture)
        return picture.astype('float32')


# ===========================================================================
# ===========================================================================



    def straight_side_down(self, colour=False, jigsaw=None):

        """
        Returns image of silhouette of piece, centred and rotated so that the straight edge is on the bottom.
        If the piece is a corner, the straight sides will be down and left.
        Only for edge and corner pieces.

        """


        N = self.N
        M = self.M
        t = self.types

        if colour:
            image = self.get_picture(jigsaw)
        else:
            image = self.thresh

        if self.piece_type == "edge":
            if t[2] == 0:
                return image, 0
            else:
                if t[3] == 0:
                    angle = -90
                elif t[0] == 0:
                    angle = 180
                else: # t[1] = 0
                    angle = 90
                    
                rmat = cv2.getRotationMatrix2D((M/2, N/2), angle, 1.0)
                rotated = cv2.warpAffine(image, rmat, (M, N))

                return rotated, angle
            
        elif self.piece_type == "corner":
            if t[1] == 0 and t[2] == 0:
                return image, 0
            else:
                if t[3] == 0 and t[2] == 0:
                    angle = -90
                elif t[0] == 0 and t[3] == 0:
                    angle = 180
                else:
                    angle = 90
                    
                rmat = cv2.getRotationMatrix2D((M/2, N/2), angle, 1.0)
                rotated = cv2.warpAffine(image, rmat, (M, N))

                return rotated, angle
            
        else:
            print("this function is for edge and corner pieces only")
            return



# ===========================================================================
# ===========================================================================



    def average_colour(self, jigsaw, cspace="BGR"):

        """
        Returns average pixel values of piece in given colour space.
        Valid colour spaces are BGR(default), HSV, HLS, H (hue), S (saturation), L (luminosity) and V (value/brightness).

        """

        
        image = jigsaw.image
        if cspace == "HSV":
            image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV_FULL)
        elif cspace == "H":
            image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV_FULL)[:,:,0]
        elif cspace == "S":
            image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV_FULL)[:,:,1]
        elif cspace == "V":
            image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV_FULL)[:,:,2]
        elif cspace == "HLS":
            image = cv2.cvtColor(image, cv2.COLOR_BGR2HLS_FULL)
        elif cspace == "L":
            image = cv2.cvtColor(image, cv2.COLOR_BGR2HLS_FULL)[:,:,1]
        # see(image)
        
        colour = image[jigsaw.piece_image == self.number]

        average = np.mean(colour, axis=0)
        return average


# ===========================================================================
# ===========================================================================


    def average_side_colour(self, jigsaw, side_index, cspace="BGR"):

        """
        Returns average pixel values of edge of piece in given colour space.
        Valid colour spaces are BGR(default), HSV, HLS, H (hue), S (saturation), L (luminosity) and V (value/brightness).

        """


        side = self.sides[side_index]
        
        disp = np.zeros(jigsaw.image.shape)
        disp = cv2.drawContours(disp, side, -1, (255,255,255), 10)
        
        pic = self.get_picture(jigsaw)
        pic = (255*pic).astype(np.uint8)
        
        if cspace == "HSV":
            pic = cv2.cvtColor(pic, cv2.COLOR_BGR2HSV_FULL)
        elif cspace == "H":
            pic = cv2.cvtColor(pic, cv2.COLOR_BGR2HSV_FULL)[:,:,0]
            disp = disp[:,:,0]
        elif cspace == "S":
            pic = cv2.cvtColor(pic, cv2.COLOR_BGR2HSV_FULL)[:,:,1]
            disp = disp[:,:,0]
        elif cspace == "V":
            pic = cv2.cvtColor(pic, cv2.COLOR_BGR2HSV_FULL)[:,:,2]
            disp = disp[:,:,0]
        elif cspace == "HLS":
            pic = cv2.cvtColor(pic, cv2.COLOR_BGR2HLS_FULL)
        elif cspace == "L":
            pic = cv2.cvtColor(pic, cv2.COLOR_BGR2HLS_FULL)[:,:,1]
            disp = disp[:,:,0]
            
        if cspace == "H" or cspace == "S" or cspace == "V" or cspace == "L":
            values = pic[np.nonzero(disp)]
            ave = np.mean(values)

        else:
            ave = []
            for i in range(3):
                arr = pic[:,:,i]
                values = arr[np.nonzero(disp[:,:,i])]
                ave.append(np.mean(values))
            ave = np.array(ave)
        
        return ave



# ===========================================================================
# ===========================================================================