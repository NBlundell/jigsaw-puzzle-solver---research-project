import numpy as np
import cv2
import matplotlib.pyplot as plt
from skimage import feature
from skimage.measure import label
from skimage.color import label2rgb
import math
import imutils
from tqdm import tqdm

from utils.helpers import *
from puzzles import puzzle_functions as pf
from puzzles.piece_class import Piece



class Jigsaw:

    """
    Class for jigsaw puzzle.
    Takes in a colour (BGR) image, and two colours.
    The two colours are the upper and lower ranges of the background colour which will be removed in order to segment the image.

    Class attributes:
    image: original input image
    piece_image: segmented and labelled image (each label corresponds to one piece of the puzzle)
    pieces: list of all pieces, using Piece class
    border: list of pieces in the border (edge and corner)
    inners: list of interior pieces
    N,M: dimensions of input image

    """



    def __init__(self, img, low_colour, high_colour, printing=False):

        """
        Performs background subtraction using input colours.
        Use the resulting mask to separate each piece.
        A list of Piece class objects is created using the segmented image.

        """

        
        self.image = img
        self.N = img.shape[0]
        self.M = img.shape[1]
        
#     threshold to remove background
        image_copy = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        kernel = np.ones((5,5),np.float32)/25
        image_copy = cv2.filter2D(image_copy,-1,kernel)
        mask = 255-cv2.inRange(image_copy, low_colour, high_colour)
        if printing:
            see(mask)

#     label each connected component - each puzzle piece
#     remove components which are too small - these are noise
        label_image = label(mask/255.0)
        labels = []
        for i in range(np.max(label_image)+1):
            c = np.count_nonzero(label_image == i)
            if c < max(self.N, self.M):
                label_image[label_image==i] = 0
            elif i > 0:
                labels.append(i)
        
        self.piece_image = label_image.astype(np.uint8)
        if printing:
            see(self.piece_image)
            
        self.pieces = []
        for p in tqdm(labels):
            try:
                self.pieces.append(Piece(img, label_image, p, show_imgs=printing))
            except:
                print("could not make piece", p)
            
        self.border = []
        self.inners = []
        for piece in self.pieces:
            if piece.piece_type == "corner":
                self.border.append(piece)
            elif piece.piece_type == "edge":
                self.border.append(piece)
            else:
                self.inners.append(piece)


# =================================================================================
# =================================================================================



    def edge_score(self, left, right, output=False, saving=False):


        """
        Get match score between two pieces. Lower is better.

        """



    # rotate images to have straight side on the bottom, or bottom and left if corner
        l, _ = left.straight_side_down()
        r, _ = right.straight_side_down()
        
        if right.piece_type == "corner":
            rmat = cv2.getRotationMatrix2D((self.M/2, self.N/2), 90, 1.0)
            r = cv2.warpAffine(r, rmat, (self.M, self.N))
        
    # find contours and get lowest point of each shape
        c1, _ = cv2.findContours(l, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        c2, _ = cv2.findContours(r, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        d1 = np.max(c1[0][:,0,1])
        d2 = np.max(c2[0][:,0,1])

    # translate left piece to left to make space for other piece, then shift up or down to be on same y level
        ty = d2-d1
        tx = int(-1* min(cv2.arcLength(c1[0], True), cv2.arcLength(c2[0], True)) /3)
        tr_matrix = np.float32([ [1,0,tx], [0,1,ty] ])
        
    # draw both piece shapes on same image, convert to BGR
        both = np.zeros(self.image.shape)
        both = cv2.warpAffine(l, tr_matrix, (l.shape[1], l.shape[0]))
        both[r==255]=255
        both = cv2.cvtColor(both,cv2.COLOR_GRAY2BGR)
        
    # find contours of new image
        contours, _ = cv2.findContours(both[:,:,0], cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        rc = []
        for i in range(len(contours)):
            if len(contours[i]) > 50:
                rc.append(contours[i])

    # get x coordinate of centres
        m1 = cv2.moments(rc[0])
        m2 = cv2.moments(rc[1])
        x1 = int(m1['m10']/m1['m00'])
        x2 = int(m2['m10']/m2['m00'])
        
    # draw lines between two pieces
        stride = int(np.abs(tx/27))
        y = min(d1, d2) - stride
        for i in range(14):
            both = cv2.line(both, (x1+20, y), (x2-20, y), (0, 0, 255), 1)
            y = y - stride
            
    # draw shapes over the lines, covering the parts of the lines that go through the pieces
        cv2.drawContours(both, rc, -1, (0, 255, 0), -1, 8)
        if output:
            see(both)
        if saving:
            cv2.imwrite("output_images/edge.png", both[300:550, 425:850, :])
        lines, _ = cv2.findContours(both[:,:,2], cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        
    # find lengths of lines
        lengths = []
        x = self.M/2 + tx/2
        xlim1 = x + self.M/18
        xlim2 = x - self.M/18
        for line in lines:
            xmean = np.mean(line[:,0,0], axis = 0)
            if xmean < xlim1 and xmean > xlim2:
                lengths.append(cv2.arcLength(line,False))
        
    # remove outliers, then compute score
        ave = np.median(lengths)
        flengths = [i for i in lengths if i >= ave/2 ]
        med = np.median(flengths)
        score = np.mean(np.abs(flengths - med))
        
        return score



# =================================================================================
# =================================================================================



    def get_border_scores(self, colour_scoring=None, cspace="BGR"):

    #     """
    #     Get table of match scores for all valid border piece combinations.
    #     Colour scoring is off by default, but can be "edge", "average" or "ave_edge"
    #     Valid colour spaces are BGR(default), HSV, HLS, H (hue), S (saturation), L (luminosity) and V (value/brightness).

    #     """

        if colour_scoring == "average":
            colours = []
            indices = []
            for i in range(len(self.border)):
                colours.append(self.border[i].average_colour(self, cspace))
                
        elif colour_scoring == "ave_edge":
            indices = []
            if cspace == "BGR" or cspace == "HSV" or cspace == "HLS":
                left_colours = np.zeros((len(self.border), 3))
                right_colours = np.zeros((len(self.border), 3))
            else:
                left_colours = np.zeros(len(self.border))
                right_colours = np.zeros(len(self.border))
            


        score_table = np.zeros((len(self.border), len(self.border)))

        for e in tqdm(range(len(self.border))):
    #     for e in range(1):
            edge = self.border[e]
        #     corner or edge
            ptype = edge.piece_type

        #     get side type of right edge
            if ptype == "edge":
                straight = np.where(np.array(edge.types)==0)[0][0]
                right_side = (straight+1) % 4
            else:
                straights = np.where(np.array(edge.types)==0)[0]
                if straights[0] == 0 and straights[1] == 3:
                    right_side = 1
                else:
                    right_side = (straights[1]+1) % 4

            stype = edge.types[right_side]

        #     get type we are looking for
            if stype == -1:
                mtype = 1
            else:
                mtype = -1
                
                
    #     colour scoring
            if colour_scoring == "ave_edge":
                colours = []
                indices = []
                if np.sum(right_colours[e]) == 0.0:
                    pcolour = edge.average_side_colour(self, stype, cspace)
                    right_colours[e] = pcolour
                else:
                    pcolour = right_colours[e]
            elif colour_scoring == "edge":
                colours = []
                indices = []
                
                
    # start getting scores between pieces
            for h in range(len(self.border)):
                if h == e:
                    score_table[e,h] = np.inf
                else:
                    other_edge = self.border[h]

        #             get left side type of this piece
                    ptype2 = other_edge.piece_type

                    if ptype2 == "edge":
                        straight = np.where(np.array(other_edge.types)==0)[0][0]
                        left_side = (straight-1) % 4
                    else:
                        straights = np.where(np.array(other_edge.types)==0)[0]
                        if straights[0] == 0 and straights[1] == 3:
                            left_side = 2
                        else:
                            left_side = (straights[0]-1) % 4

                    otype = other_edge.types[left_side]

                    if ptype == "corner" and ptype2 == "corner":
                        score_table[e,h] = np.inf
                    elif otype != mtype:
                        score_table[e,h] = np.inf
                    else:
    #                     possible match
                        score_table[e,h] = self.edge_score(edge, other_edge)
                        
                        if colour_scoring != None:
                            indices.append(h)
                            if colour_scoring == "ave_edge":
                                if np.sum(left_colours[h]) == 0.0:
                                    ncolour = other_edge.average_side_colour(self, otype, cspace)
                                    left_colours[h] = ncolour
                                else:
                                    ncolour = left_colours[h]
                                colours.append(np.sum(np.abs(pcolour - ncolour)))
                            elif colour_scoring == "edge":
                                colours.append(self.edge_colour_score(edge, other_edge, stype, otype, cspace))
                
            if colour_scoring != None:
                if colour_scoring == "average":
                    diffs = np.sum(np.abs(np.array(colours)[indices] - colours[e]))
                    sort = np.argsort(diffs)
                elif colour_scoring == "edge":
                    sort = np.argsort(colours)[::-1]
                else:
                    sort = np.argsort(colours)
                best = np.array(indices)[sort]
                
                for i in range(len(best)):
                    ind = best[i]
                    score_table[e,ind] *= (1 - (1/2**(i+2)))
                

        return score_table  



# =================================================================================
# =================================================================================


    def edge_colour_score(self, piece1, piece2, side1, side2, cspace="BGR", alpha=0.5):


        """
        Get score from difference between colour at edge of pieces.
        Valid colour spaces are BGR(default), HSV, HLS, H (hue), S (saturation), L (luminosity) and V (value/brightness).

        """



        pic1 = piece1.get_picture(self)
        pic2 = piece2.get_picture(self)

        contour1 = piece1.sides[side1]
        contour2 = piece2.sides[side2]

        disp = np.zeros(self.image.shape)
        disp = cv2.drawContours(disp, contour1, -1, (255,255,255), 1)
        n1 = np.count_nonzero(disp[:,:,0])

        disp = np.zeros(self.image.shape)
        disp = cv2.drawContours(disp, contour2, -1, (255,255,255), 1)
        n2 = np.count_nonzero(disp[:,:,0])

        offset = int(np.abs(n1-n2)/2)

        if cspace == "HSV" or cspace == "HLS" or cspace == "BGR":
            line_image = np.zeros((min(n1,n2), 2, 3))
            l = 3
        else:
            line_image = np.zeros((min(n1,n2), 2))
            l = 1

        c1, _ = pf.sort_contour(contour1)
        c2, _ = pf.sort_contour(contour2)

        if cspace == "HSV":
            pic1 = cv2.cvtColor(pic1, cv2.COLOR_BGR2HSV_FULL)
            pic2 = cv2.cvtColor(pic2, cv2.COLOR_BGR2HSV_FULL)
            num_channels = 3
        elif cspace == "H":
            pic1 = cv2.cvtColor(pic1, cv2.COLOR_BGR2HSV_FULL)[:,:,0]
            pic2 = cv2.cvtColor(pic2, cv2.COLOR_BGR2HSV_FULL)[:,:,0]
            num_channels = 1
        elif cspace == "S":
            pic1 = cv2.cvtColor(pic1, cv2.COLOR_BGR2HSV_FULL)[:,:,1]
            pic2 = cv2.cvtColor(pic2, cv2.COLOR_BGR2HSV_FULL)[:,:,1]
            num_channels = 1
        elif cspace == "V":
            pic1 = cv2.cvtColor(pic1, cv2.COLOR_BGR2HSV_FULL)[:,:,2]
            pic2 = cv2.cvtColor(pic2, cv2.COLOR_BGR2HSV_FULL)[:,:,2]
            num_channels = 1
        elif cspace == "HLS":
            pic1 = cv2.cvtColor(pic1, cv2.COLOR_BGR2HLS_FULL)
            pic2 = cv2.cvtColor(pic2, cv2.COLOR_BGR2HLS_FULL)
            num_channels = 3
        elif cspace == "L":
            pic1 = cv2.cvtColor(pic1, cv2.COLOR_BGR2HLS_FULL)[:,:,1]
            pic2 = cv2.cvtColor(pic2, cv2.COLOR_BGR2HLS_FULL)[:,:,1]
            num_channels = 1
        else:
            num_channels = 3


        for i in range(min(n1, n2)):
            if n2 > n1:
                x1 = contour1[i][0][0]
                y1 = contour1[i][0][1]
                
                x2 = contour2[i+offset][0][0]
                y2 = contour2[i+offset][0][1]
            else:
                x1 = contour1[i+offset][0][0]
                y1 = contour1[i+offset][0][1]
                
                x2 = contour2[i][0][0]
                y2 = contour2[i][0][1]
                

            if l == 3:
                for j in range(3):
                    line_image[i, 0, j] = pic1[y1,x1,j]
                    line_image[i, 1, j] = pic2[y2,x2,j]
            else:
                line_image[i, 0] = pic1[y1,x1]
                line_image[i, 1] = pic2[y2,x2]


        line_image = (255*line_image).astype("uint8")

        sobelx = np.array([ [-1,1], [-2,2], [-1,1] ])

        n = min(n1,n2)
        output = np.zeros((n-2, num_channels))
        for i in range(n-2):
            if num_channels == 3:
                output[i, 0] = (np.sum(sobelx*line_image[i:(i+3), :, 0]))**2
                output[i, 1] = (np.sum(sobelx*line_image[i:(i+3), :, 1]))**2
                output[i, 2] = (np.sum(sobelx*line_image[i:(i+3), :, 2]))**2
            else:
                output[i] = (np.sum(sobelx*line_image[i:(i+3)]))**2

        ms = np.sum(output, axis=0)/(n-2)

        score = 1/( 1 + alpha*np.sqrt(np.sum(ms)) )

        return score



# =================================================================================
# =================================================================================