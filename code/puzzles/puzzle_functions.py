import numpy as np
import cv2
import matplotlib.pyplot as plt
from skimage import feature
from skimage.measure import label
from skimage.color import label2rgb
import math
import imutils
from utils.helpers import *


# ===============================================================================================
# ===============================================================================================



def translate_to_centre(img, cx, cy):

    
# amount to translate by
    dx = 0.5*img.shape[0] - cy
    dy = 0.5*img.shape[1] - cx
    
# translate image
    translation_matrix = np.float32([ [1,0,dy], [0,1,dx] ])
    t1 = cv2.warpAffine(img, translation_matrix, (img.shape[1], img.shape[0]))
    
    return t1


# ===============================================================================================
# ===============================================================================================


def get_angle(contour):
# get bounding rotated rectangle
# rect[2] is the angle of rotation in degrees
    rect = cv2.minAreaRect(contour)
    
# find angle to rotate piece by, want image to maintain original orientation (portrait or landscape)
    a1 = abs(rect[2])
    a2 = 90+abs(rect[2])
    if a1 < 135 and a1 >= 90:
        angle = a2
    elif a2 < 135 and a2 >= 90:
        angle = a1
    else:
        angle = a2

    return angle



# ===============================================================================================
# ===============================================================================================



def rotate_img(img, angle):

# rotate
    rotated = imutils.rotate_bound(img, angle)

# crop image to original size
    size = rotated.shape
    s1 = int((size[0] - img.shape[0])/2)
    s2 = int((size[1] - img.shape[1])/2)
    
    final = rotated[ s1:img.shape[0]+s1 , s2:img.shape[1]+s2 ]

    return final


# ===============================================================================================
# ===============================================================================================


# go through corners and for each point, check if it has a pair
# a pair is another point which is very close in value on one dimension, but far away on another

def get_good_corners(fcorners):
    cmin = np.min(fcorners,axis=0)
    cmax = np.max(fcorners, axis=0)
    cmean = np.mean(fcorners, axis=0)

    fcorn = []
    good = np.zeros(len(fcorners))

    for i in range(len(fcorners)):
        p = fcorners[i]
        for j in range(i+1, len(fcorners)):
            h = fcorners[j]
            if np.abs(p[0] - h[0]) < 10 and np.abs(p[1] - h[1]) > 20:
                good[i] = 1
                good[j] = 1
            elif np.abs(p[1] - h[1]) < 10 and np.abs(p[0] - h[0]) > 20:
                good[i] = 1
                good[j] = 1
                
    if np.count_nonzero(good==1) > 4:
        for w in range(len(good)):
            p = fcorners[w]
            for v in range(w+1, len(fcorners)):
                if good[v] == 1:
                    d = fcorners[v]
                    if np.abs(p[0] - d[0]) < 10 and np.abs(p[1] - d[1]) < 10:
                        good[v] = 0
    
    for k in range(len(good)):
        if good[k] == 1:
            fcorn.append(fcorners[k])
    
    return fcorn


# ===============================================================================================
# ===============================================================================================


def order_sides(sides):
    s1 = sides[0]
    s2 = sides[1]
    s3 = sides[2]
    s4 = sides[3]
    
# sort into up, left, down and right

# get average y values for each side
    y1 = np.mean(s1[:,0,1], axis = 0)
    y2 = np.mean(s2[:,0,1], axis = 0)
    y3 = np.mean(s3[:,0,1], axis = 0)
    y4 = np.mean(s4[:,0,1], axis = 0)
    

# up side will have lowest y mean, down side will have greatest y mean
    up = np.argmin([y1,y2,y3,y4])
    upside = sides[up]
    down = np.argmax([y1,y2,y3,y4])
    downside = sides[down]
    
# same for x means, and left and right sides
    x1 = np.mean(s1[:,0,0], axis = 0)
    x2 = np.mean(s2[:,0,0], axis = 0)
    x3 = np.mean(s3[:,0,0], axis = 0)
    x4 = np.mean(s4[:,0,0], axis = 0)
    
    left = np.argmin([x1,x2,x3,x4])
    leftside = sides[left]
    right = np.argmax([x1,x2,x3,x4])
    rightside = sides[right]
    
    sides_ordered = [upside, leftside, downside, rightside]

    return sides_ordered



# ===============================================================================================
# ===============================================================================================



def side_types(sides, size):
    types = []
    
    for i in range(4):
        s = sides[i]
        dim = (i+1)%2
        
        # for HORIZONTAL sides, check diffs in y direction
        # max will be BELOW mean, min above
        # for VERTICAL sides, check x direction
        # max will be to right of mean, min to left
        # if either diff is small (less than cutoff value), straight edge
        
        m = np.mean(s[:,0,dim])
        diff_max = np.max(s[:,0,dim]) - m
        diff_min = m - np.min(s[:,0,dim])
        
        cutoff = 0.008*size

        if diff_min < cutoff or diff_max < cutoff:
#              straight
            types.append(0)
        elif diff_max > diff_min:
            if i == 0 or i == 1:
#                indent
                types.append(-1)
            else:
#                 outdent
                types.append(1)
        else:
            if i == 0 or i == 1:
#                 outdent
                types.append(1)
            else:
#                 indent
                types.append(-1)
        
    return types



# ===============================================================================================
# ===============================================================================================


def get_sides(img, printing=False, saving=False):

# perform closing with square SE to remove indents and straighten sides/sharpen corners slightly
    dims = (img.shape[0], img.shape[1], 3)
    k = 43
    morph = cv2.morphologyEx(img, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT,(k,k)))
    if saving:
        see(morph)
        cv2.imwrite("output_images/img1.png", morph[315:565, 625:875])
    
# find harris corners of closed shape, then keep only most confident points
    vis = np.zeros(dims)
    corn = cv2.cornerHarris(morph,7,5,0.05)
    rate = 0.3
    vis[corn > rate*np.max(corn)]=[0,0,255]

    if printing:
        see(vis)
    if saving:
        cv2.imwrite("output_images/img2.png", vis[315:565, 625:875,:])
    
    corners = np.where(vis[:,:,2] ==255)
    corners = np.stack([corners[0], corners[1]], axis=1)
    

# find convex hull of closed shape
    co, _ = cv2.findContours(morph, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    d = np.zeros(dims, np.uint8)
    cv2.drawContours(d, co, -1, (0, 255, 0), 1, 8)
    hull = [(cv2.convexHull(co[0], False))]
    cv2.drawContours(vis, hull, 0, 255, 4, 8)
    if printing:
        see(vis)
    if saving:
        cv2.imwrite("output_images/img3.png", vis[315:565, 625:875,:])
    

# keep only the corners which intersect the convex hull
    fcorners = []
    nvis = np.zeros(vis.shape)
    for p in corners:
        if vis[p[0], p[1],0] == 255 and nvis[p[0], p[1],1] == 0:
            fcorners.append(p)
            nvis = cv2.circle(nvis, (p[1],p[0]), 5, (255,255,255), -1)
            
    if printing:
        see(nvis)
    if saving:
        cv2.imwrite("output_images/img4.png", nvis[315:565, 625:875,:])
    
# if there are still more than 4 corners, use function to reduce to real ones
    if len(fcorners) > 4:
        good_corners = get_good_corners(fcorners)
    else:
        good_corners = fcorners
    
# draw original shape outline and corners
    contours, _ = cv2.findContours( img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    back = np.zeros(dims)
    cv2.drawContours( back, contours, 0, (255,255,0), 1);
    
    for p in good_corners:
        if saving or printing:
            back = cv2.circle(back, (p[1],p[0]), 2, (0,0,255), -1)
        cv2.putText(back, ".", (p[1]-15,p[0]+5), cv2.FONT_HERSHEY_SIMPLEX, 2.9, (0,0,255), 2)

    
# split contours into 4 sides using the corners
    colours = [[255,255,0], [255,0,0], [0,255,0], [0,0,255], [255,255,0]]
    ci = 0
    count = 0
    changes = []
    for i in range(len(contours[0])):
        count = count + 1
        x = contours[0][i][0][1]
        y = contours[0][i][0][0]
        if back[x,y][2] == 255 and count > 15:
            ci = ci+1
            count = 0
            changes.append(i)
        if ci < 5:
            back[x,y] = colours[ci]
    if len(changes) == 3:
        changes.insert(0, 10)

    if printing:
        see(back)
    if saving:
        cv2.imwrite("output_images/img5.png", back[315:565, 625:875,:])
    
    s1 = np.concatenate((contours[0][:changes[0]], contours[0][changes[3]:len(contours[0])]))
    s2 = contours[0][changes[0]:changes[1]]
    s3 = contours[0][changes[1]:changes[2]]
    s4 = contours[0][changes[2]:changes[3]]
    
    return [s1,s2,s3,s4]



# ===============================================================================================
# ===============================================================================================


def sort_contour(contour):

    xmin = np.argmin(contour[:,0,0])
    xmax = np.argmax(contour[:,0,0])
    ymax = np.argmax(contour[:,0,1])
    ymin = np.argmin(contour[:,0,1])
    
    ydiff = np.max(contour[:,0,1]) - np.min(contour[:,0,1])
    xdiff = np.max(contour[:,0,0]) - np.min(contour[:,0,0])
    
    if xdiff > ydiff:
        dim = 0
        contour = np.vstack([contour[xmax:], contour[0:xmax]])
        if contour[len(contour)-10][0][dim] < contour[10][0][dim]:
            contour = np.flip(contour,0)
    else:
        dim = 1
        contour = np.vstack([contour[ymax:], contour[0:ymax]])
        if contour[len(contour)-10][0][dim] < contour[10][0][dim]:
            contour = np.flip(contour,0)

    return contour, dim



# ===============================================================================================
# ===============================================================================================



def fit_ellipse(contour, img_shape):
    
# this is a mess

    contour, dim = sort_contour(contour)

    min_length = int(0.025*len(contour))

    dx1 = 0
    prevX = 0
    incX = False
    nx = 0

    ep1 = 0
    ep2 = 0
    
    poly = np.zeros(img_shape).astype(np.uint8)
    cv2.drawContours(poly, contour, -1, (0,255,0), 1)
    recording = False

    contour = contour[10:-10]
    for ind,c in enumerate(contour):
        x = c[0][dim]

        dx1 = x - prevX
        prevX = x
        
        if recording:
            poly[c[0][1],c[0][0]] = [255.0,255.0,255.0]

        if dx1 < 0:
            if incX:
                incX = False
                if nx > min_length:
                    if ep1 == 0:
                        ep1 = ind
                nx = 0

        elif dx1 > 0:
            if not incX:
                incX = True
                nx = 0
                
        elif dx1 == 0 and ep1 != 0:
            if nx > min_length:
                ep2 = ind
                if not recording:
                    recording = True
                else:
                    recording = False
                index = int((ep1 + ep2)/2)
                i = contour[index][0][0]
                j = contour[index][0][1]
                cv2.putText(poly, ".", (i,j), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,255), 2)
                ep1 = 0
                ep2 = 0
                incX = True
                nx = 0
        elif dx1 == 0 and not incX:
            nx = 0

        nx = nx + 1
        
    cnts, _ = cv2.findContours( poly[:,:,0].copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    if len(cnts) > 1:
        for i in range(len(cnts)):
            if len(cnts[i]) > 10:
                c = cnts[i]
    else:
        c = cnts[0]
        
    ellipse = cv2.fitEllipse(c)
    w = ellipse[1][0]
    h = ellipse[1][1]
    if w > 0.5*len(contour) or h > 0.5*len(contour) or w > 2*h or h > 2*w \
    or w < 0.1*len(contour) or h < 0.1*len(contour) :
        print("APPROX")
        a = int(len(contour)/3)
        b = int(2*len(contour)/3)
        ellipse = cv2.fitEllipse(contour[a:b])
    
    cv2.ellipse(poly,ellipse,(255,0,255),1)
    # see(poly)

    return ellipse

    # ellipse = ((x_centre, y_centre), (width, height), angle of rotation)




# ===============================================================================================
# ===============================================================================================



def cycle(table, start, n):

    # get ordering of border with given start, using table of scores

    ordering = [start]
    current_piece = start
    t = 0

    while len(ordering) < n and t < 400:
        t = t + 1
        next_piece = np.argmin(table[current_piece,:])

        if next_piece in ordering and len(ordering) < n:
            current_piece = ordering[-1]
            table[current_piece,next_piece] = np.inf
        else:
            ordering.append(next_piece)
            current_piece = next_piece
            
    return ordering


# ===============================================================================================
# ===============================================================================================


def get_border_ordering(scores, border_length):

    # get an ordering of the border, try different starts until it includes all the pieces
    
    starts = np.random.choice(range(border_length), border_length, replace=False)
    start = 0
    n = 0
    
    while n < border_length and start < border_length:
        table = np.copy(scores)
        border = cycle(table, starts[start], border_length+1)
        n = len(border)
        start = start + 1
        
    return border


# ===============================================================================================
# ===============================================================================================


def ellipse_point_given_angle(a, b, angle):
# get point on ellipse given angle in degrees (clockwise)
    
    x = (a*b) / np.sqrt( b**2 + (a**2)*(math.tan(math.radians(angle)))**2 )

    if math.tan(math.radians(angle)) == 0.0:
        y = 0.0
    else:
        y = (a*b) / np.sqrt( a**2 + (b**2)/(math.tan(math.radians(angle)))**2 )

    if angle < -90 or angle > 90:
        x = -1*np.abs(x)
    else:
        x = np.abs(x)

    if angle > 0 and angle < 180:
        y = np.abs(y)
    else:
        y = -1*np.abs(y)

    return (x, y)


# ===============================================================================================
# ===============================================================================================


def get_xs_and_ys(contour):
# the way contour points are stored is terrible
    
    kx = contour[:,0,0]
    ky = contour[:,0,1]

    a = np.column_stack([kx,ky])
    return a


# ===============================================================================================
# ===============================================================================================


def get_closest_point(contour, point):
    
    min_dist = np.min(np.sum(np.abs(contour - point), axis=1))
    
    if min_dist > 3:
        return -1
    else:
        return np.argmin(np.sum(np.abs(contour - point), axis=1))


# ===============================================================================================
# ===============================================================================================


def distance(point1, point2):
# euclidean distance

    xd = (point1[0] - point2[0])**2
    yd = (point1[1] - point2[1])**2
    
    d = np.sqrt(xd + yd)
    return d


# ===============================================================================================
# ===============================================================================================



def join_images(pic2, pic1):

    sil1 = np.zeros(pic1.shape)
    sil1[pic1 > 0] = 1
    sil2 = np.zeros(pic1.shape)
    sil2[pic2 > 0] = 1
    sil2 = sil2 - sil1

    pic1[sil2 > 0] = 1.0
    pic2[sil1 > 0] = 1.0

    both = pic1*pic2
    
    return both


# ===============================================================================================
# ===============================================================================================



def ellipse_score(jigsaw, piece1, piece2, ellipse1, ellipse2, s1, s2, printing=False, move_imgs=False):
    
    N = jigsaw.N
    M = jigsaw.M
    
    side1 = piece1.sides[s1]
    c1 = ellipse1[0]
    angle1 = ellipse1[2]

    side2 = piece2.sides[3]
    c2 = ellipse2[0]
    angle2 = ellipse2[2]

# major and minor axis lengths for both ellipses
    a1 = ellipse1[1][0]/2
    b1 = ellipse1[1][1]/2
    a2 = ellipse2[1][0]/2
    b2 = ellipse2[1][1]/2

    
# rotate edges using angle from ellipse
# then translate to centre of image, using ellipse centre as reference point
    disp1 = np.zeros(jigsaw.image.shape[:2], dtype="uint8")
    disp2 = np.zeros(jigsaw.image.shape[:2], dtype="uint8")

    cv2.drawContours(disp1, side1, -1, (255,255,255), 1)
    cv2.drawContours(disp2, side2, -1, (255,255,255), 1)

# rotate 
    centre = (M/2, N/2)
    r1 = cv2.getRotationMatrix2D(centre, -angle1, 1.0)
    disp1 = cv2.warpAffine(disp1, r1, (M,N))
    r2 = cv2.getRotationMatrix2D(centre, -angle2, 1.0)
    disp2 = cv2.warpAffine(disp2, r2, (M,N))

# translate
# first get new centres of ellipses (which will be the same as the tab in the edge)
# after the rotation
    nc1 = np.dot(r1, np.array([c1[0], c1[1], 1]))
    nc2 = np.dot(r2, np.array([c2[0], c2[1], 1]))
    
# then translate
    disp1 = translate_to_centre(disp1, nc1[0], nc1[1])
    disp2 = translate_to_centre(disp2, nc2[0], nc2[1])

# find contours of transformed edges
    con1, _ = cv2.findContours( disp1, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    con2, _ = cv2.findContours( disp2, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

# new ellipses with zero rotation, centred at image centre
    e1 = (centre, ellipse1[1], 0.0)
    e2 = (centre, ellipse2[1], 0.0)

    if printing:
        test = np.zeros(img.shape)
        test = cv2.drawContours(test, con1, -1, (0,255,0), 1)
        test = cv2.drawContours(test, con2, -1, (255,0,255), 1)
        cv2.ellipse(test, e1, (255,255,255), 1)
        cv2.ellipse(test, e2, (255,255,255), 1)
        see(test)
        
    if move_imgs:
        pic1 = piece1.get_picture(jigsaw)
        pic1 = cv2.warpAffine(pic1, r1, (M,N))
        pic1 = translate_to_centre(pic1, nc1[0], nc1[1])

        pic2 = piece2.get_picture(jigsaw)
        pic2 = cv2.warpAffine(pic2, r2, (M,N))
        pic2 = translate_to_centre(pic2, nc2[0], nc2[1])
        
        look(join_images(pic1, pic2))


#     change contours to a more managable format
    con1 = get_xs_and_ys(con1[0])
    con2 = get_xs_and_ys(con2[0])
    n1 = con1.shape[0]
    n2 = con2.shape[0]

# get ellipse point at 0 degrees
# if that turns out to be on the wrong side, use 180 degrees
    start_angle = 0.0

    p1 = ellipse_point_given_angle(a1,b1,start_angle)
    p1 = (int(p1[0] + centre[0]), int(p1[1] + centre[1]))

# get point on contour with the same coords as ellipse point
# or closest point
    start1 = get_closest_point(con1, p1)
    
# if there is no close point, try with 180 degree angle
    if start1 == -1:
        start_angle = 180.0
        p1 = ellipse_point_given_angle(a1,b1,start_angle)
        p1 = (int(p1[0] + centre[0]), int(p1[1] + centre[1]))

# get corresponding point on second side
    p2 = ellipse_point_given_angle(a2,b2,start_angle)
    p2 = (int(p2[0] + centre[0]), int(p2[1] + centre[1]))

    start1 = get_closest_point(con1, p1)
    start2 = get_closest_point(con2, p2)
    
    if start2 == -1:
        start_angle = np.abs(start_angle-180.0)
        p2 = ellipse_point_given_angle(a2,b2,start_angle)
        p2 = (int(p2[0] + centre[0]), int(p2[1] + centre[1]))
    start2 = get_closest_point(con2, p2)

# start adding up to score
    score = 0.0

    iter1 = start1
    iter2 = start2

# forward from starting point
    for i in range(np.min([n1-start1, n2-start2])):
        d = distance(con1[iter1], con2[iter2])
        score += d**3
        iter1 += 1
        iter2 += 1

    forward_score = score/i

    iter1 = start1
    iter2 = start2
    score = 0.0
    
# backward from starting point
    for j in range(max(start1, start2), 0, -1):
        if iter1 >= 0 and iter2 >= 0:
            d = distance(con1[iter1], con2[iter2])
            score += d**3
            iter1 -= 1
            iter2 -= 1
        else:
            break

    backward_score = score/( max(start1, start2) - j )

# get average
    total_score = (forward_score + backward_score)/2
    
    return total_score


# ===============================================================================================
# ===============================================================================================


def move_ellipse(piece, angle, dx, dy, side, jigsaw):
# get translated and rotated centre of ellipse

    ptype = piece.piece_type

    if side == "right":
        if ptype == "edge":
            straight = np.where(np.array(piece.types)==0)[0][0]
            index = (straight+1) % 4
        else:
            straights = np.where(np.array(piece.types)==0)[0]
            if straights[0] == 0 and straights[1] == 3:
                index = 1
            else:
                index = (straights[1]+1) % 4
    else:
        if ptype == "edge":
            straight = np.where(np.array(piece.types)==0)[0][0]
            index = (straight-1) % 4
        else:
            straights = np.where(np.array(piece.types)==0)[0]
            if straights[0] == 0 and straights[1] == 3:
                index = 2
            else:
                index = (straights[0]-1) % 4
            
    ellipse = fit_ellipse(piece.sides[index], jigsaw.image.shape)
    
    r1 = cv2.getRotationMatrix2D((jigsaw.M/2, jigsaw.N/2), angle, 1.0)
    r_centre = np.dot(r1, np.array([ellipse[0][0], ellipse[0][1], 1]))
    tr_matrix = np.float32([ [1,0,dx], [0,1,dy] ])
    new_centre = np.dot(tr_matrix, np.array([r_centre[0], r_centre[1], 1]))
    
    return new_centre


# ===============================================================================================
# ===============================================================================================


def place_border(ordering, full_image, first, cx, cy, jigsaw, saving):

    angle_offset = 0
    
    N = full_image.shape[0]
    M = full_image.shape[1]
    
    for i in range(1, len(jigsaw.border)):

        second = jigsaw.border[ordering[i]]
        
        if second.piece_type == "corner":
            angle_offset += 90
        
        pic, angle = second.straight_side_down(colour=True, jigsaw=jigsaw)
        pic = cv2.copyMakeBorder(pic,200,200,200,200,cv2.BORDER_CONSTANT,value=0)
        
        if np.abs(angle_offset) > 0 and angle_offset != 360:
            angle = angle + angle_offset
            rmat = cv2.getRotationMatrix2D((M/2, N/2), angle_offset, 1.0)
            pic = cv2.warpAffine(pic, rmat, (M, N))
        
        (currx, curry) = move_ellipse(second, angle, 0, 0, "left", jigsaw)
        
        dx = cx - currx
        dy = cy - curry

        translation_matrix = np.float32([ [1,0,dx], [0,1,dy] ])
        tr = cv2.warpAffine(pic, translation_matrix, (M, N))
        
        full_image = join_images(np.copy(full_image), np.copy(tr))
        see(full_image)
        if saving:
            cv2.imwrite("output_images/border" + str(i) + ".png", im2cv(full_image))
        
        (cx,cy) = move_ellipse(second, angle, dx, dy, "right", jigsaw)


# ===============================================================================================
# ===============================================================================================


def assemble_border(jigsaw, ordering, saving=False):
    
# make ordering start from corner
    if jigsaw.border[ordering[0]].piece_type == "edge":
        for i in range(len(ordering)):
            if jigsaw.border[ordering[i]].piece_type == "corner":
                temp = ordering[i:]
                temp.extend(ordering[:i])
                break
        ordering = temp
    
# place first piece
    first = jigsaw.border[ordering[0]]
    pic, angle = first.straight_side_down(colour=True, jigsaw=jigsaw)

# pad image in case border goes off
    pic = cv2.copyMakeBorder(pic,200,200,200,200,cv2.BORDER_CONSTANT,value=0)
    
# translate to lower left
    cx = 0.5*pic.shape[0]
    cy = 0.5*pic.shape[1]

    lcy = 6*(pic.shape[1]/(len(jigsaw.border) + len(jigsaw.inners)))
    lcx = 8*(pic.shape[0]/(len(jigsaw.border) + len(jigsaw.inners)))

    dx = lcy - cy
    dy = cx - lcx

# translate image
    translation_matrix = np.float32([ [1,0,dx], [0,1,dy] ])
    tr1 = cv2.warpAffine(pic, translation_matrix, (pic.shape[1], pic.shape[0]))
    
    (cx,cy) = move_ellipse(first, angle, dx, dy, "right", jigsaw)
    
# rest of the border
    place_border(ordering, tr1, first, cx, cy, jigsaw, saving)


# ===============================================================================================
# ===============================================================================================